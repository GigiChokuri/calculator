package com.example.calculator

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import net.objecthunter.exp4j.ExpressionBuilder


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        ggOne.setOnClickListener { appendOnExpresstion("1", true) }
        ggTwo.setOnClickListener { appendOnExpresstion("2", true) }
        ggThree.setOnClickListener { appendOnExpresstion("3", true) }
        ggFour.setOnClickListener { appendOnExpresstion("4", true) }
        ggFive.setOnClickListener { appendOnExpresstion("5", true) }
        ggSix.setOnClickListener { appendOnExpresstion("6", true) }
        ggSeven.setOnClickListener { appendOnExpresstion("7", true) }
        ggEight.setOnClickListener { appendOnExpresstion("8", true) }
        ggNine.setOnClickListener { appendOnExpresstion("9", true) }
        ggZero.setOnClickListener { appendOnExpresstion("0", true) }
        ggDot.setOnClickListener { appendOnExpresstion(".", true) }


        ggPlus.setOnClickListener { appendOnExpresstion("+", false) }
        ggMinus.setOnClickListener { appendOnExpresstion("-", false) }
        ggMul.setOnClickListener { appendOnExpresstion("*", false) }
        ggDivide.setOnClickListener { appendOnExpresstion("/", false) }
        ggOpen.setOnClickListener { appendOnExpresstion("(", false) }
        ggClose.setOnClickListener { appendOnExpresstion(")", false) }


        ggClear.setOnClickListener {
            ggExpression.text = " "
            ggResult.text = " "

        }

        ggBack.setOnClickListener{
            val string = ggExpression.text.toString()
            if (string.isNotEmpty()) {
                ggExpression.text = string.substring(0, string.length-1)
            }
            ggResult.text = " "


        }

        ggEquals.setOnClickListener {
            try {
                val expression = ExpressionBuilder(ggExpression.text.toString()).build()
                val result = expression.evaluate()
                val longResult = result.toLong()
                if (result == longResult.toDouble())
                    ggResult.text = longResult.toString()
                else
                    ggResult.text = result.toString()

            }catch (x:Exception) {
                Log.d("Exception", " message : " + x.message)

            }
        }






    }

    fun appendOnExpresstion ( string: String, canClear : Boolean){
        if(canClear) {
            ggResult.text = ""
            ggExpression.append(string)

        }else{
            ggExpression.append(ggResult.text)
            ggExpression.append(string)
            ggResult.text= ""




        }
    }





}